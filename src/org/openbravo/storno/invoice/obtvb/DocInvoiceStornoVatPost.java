/************************************************************************************ 
 * Copyright (C) 2011-2016 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************/
package org.openbravo.storno.invoice.obtvb;

import java.math.BigDecimal;
import java.sql.Connection;

import javax.servlet.ServletException;

import org.apache.log4j.Logger;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.erpCommon.ad_forms.AcctSchema;
import org.openbravo.erpCommon.ad_forms.AcctServer;
import org.openbravo.erpCommon.ad_forms.DocInvoice;
import org.openbravo.erpCommon.ad_forms.DocInvoiceTemplate;
import org.openbravo.erpCommon.ad_forms.DocLine;
import org.openbravo.erpCommon.ad_forms.DocLine_FinPaymentSchedule;
import org.openbravo.erpCommon.ad_forms.DocLine_Invoice;
import org.openbravo.erpCommon.ad_forms.DocTax;
import org.openbravo.erpCommon.ad_forms.Fact;
import org.openbravo.erpCommon.ad_forms.FactLine;
import org.openbravo.erpCommon.ad_forms.ProductInfo;
import org.openbravo.erpCommon.utility.SequenceIdData;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.obtvb.utillity.OBTVB_Utility;

public class DocInvoiceStornoVatPost extends DocInvoiceTemplate {

  @SuppressWarnings("unused")
  private static final long serialVersionUID = 1L;
  static Logger log4j = Logger.getLogger(DocInvoiceStornoVatPost.class);
  String SeqNo = "0";

  public Fact createFact(DocInvoice docInvoice, AcctSchema as, ConnectionProvider conn,
      Connection con, VariablesSecureApp vars) throws ServletException {
    try {
      OBContext.setAdminMode(true);
      final Organization org = (Organization) OBDal.getInstance().get(Organization.class,
          docInvoice.AD_Org_ID);

      DocLine_FinPaymentSchedule[] m_payments = docInvoice.getM_payments();

      // create Fact Header
      Fact fact = new Fact(docInvoice, as, Fact.POST_Actual);
      String Fact_Acct_Group_ID = SequenceIdData.getUUID();
      // Cash based accounting
      if (!as.isAccrual())
        return null;

      /** @todo Assumes TaxIncluded = N */

      // STORNO_ARS
      if (docInvoice.DocumentType.equals("STORNO_ARS")) {
        log4j.debug("Point 1");
        // Receivables DR
        if (m_payments == null || m_payments.length == 0)
          for (int i = 0; docInvoice.m_debt_payments != null
              && i < docInvoice.m_debt_payments.length; i++) {
            if (docInvoice.m_debt_payments[i].getIsReceipt().equals("Y"))
              fact.createLine(
                  docInvoice.m_debt_payments[i],
                  docInvoice.getAccountBPartner(docInvoice.C_BPartner_ID, as, true,
                      docInvoice.m_debt_payments[i].getDpStatus(), conn),
                  docInvoice.C_Currency_ID,
                  new BigDecimal(AcctServer.getConvertedAmt(
                      docInvoice.m_debt_payments[i].getAmount(),
                      docInvoice.m_debt_payments[i].getC_Currency_ID_From(),
                      docInvoice.C_Currency_ID, docInvoice.DateAcct, "", conn)).negate().toString(),
                  "", Fact_Acct_Group_ID, docInvoice.nextSeqNo(SeqNo), docInvoice.DocumentType,
                  conn);
            else
              fact.createLine(
                  docInvoice.m_debt_payments[i],
                  docInvoice.getAccountBPartner(docInvoice.C_BPartner_ID, as, false,
                      docInvoice.m_debt_payments[i].getDpStatus(), conn),
                  docInvoice.C_Currency_ID,
                  "",
                  new BigDecimal(AcctServer.getConvertedAmt(
                      docInvoice.m_debt_payments[i].getAmount(),
                      docInvoice.m_debt_payments[i].getC_Currency_ID_From(),
                      docInvoice.C_Currency_ID, docInvoice.DateAcct, "", conn)).negate().toString(),
                  Fact_Acct_Group_ID, docInvoice.nextSeqNo(SeqNo), docInvoice.DocumentType, conn);
          }
        else
          for (int i = 0; m_payments != null && i < m_payments.length; i++) {
            fact.createLine(
                m_payments[i],
                docInvoice.getAccountBPartner(docInvoice.C_BPartner_ID, as, true, false, conn),
                docInvoice.C_Currency_ID,
                new BigDecimal(AcctServer.getConvertedAmt(m_payments[i].getAmount(),
                    m_payments[i].getC_Currency_ID_From(), docInvoice.C_Currency_ID,
                    docInvoice.DateAcct, "", conn)).negate().toString(), "", Fact_Acct_Group_ID,
                docInvoice.nextSeqNo(SeqNo), docInvoice.DocumentType, conn);
            fact.createLine(
                m_payments[i],
                docInvoice.getAccountBPartner(docInvoice.C_BPartner_ID, as, true, true, conn),
                docInvoice.C_Currency_ID,
                new BigDecimal(AcctServer.getConvertedAmt(m_payments[i].getPrepaidAmount(),
                    m_payments[i].getC_Currency_ID_From(), docInvoice.C_Currency_ID,
                    docInvoice.DateAcct, "", conn)).negate().toString(), "", Fact_Acct_Group_ID,
                docInvoice.nextSeqNo(SeqNo), docInvoice.DocumentType, conn);
          }
        // Charge CR
        log4j.debug("The first create line");
        fact.createLine(null, docInvoice.getAccount(AcctServer.ACCTTYPE_Charge, as, conn),
            docInvoice.C_Currency_ID, "",
            new BigDecimal(docInvoice.getAmount(AcctServer.AMTTYPE_Charge)).negate().toString(),
            Fact_Acct_Group_ID, docInvoice.nextSeqNo(SeqNo), docInvoice.DocumentType, conn);
        // TaxDue CR
        log4j.debug("docInvoice.getM_taxes().length: " + docInvoice.getM_taxes());
        for (int i = 0; docInvoice.getM_taxes() != null && i < docInvoice.getM_taxes().length; i++) {
          // New docLine created to assign C_Tax_ID value to the entry
          DocLine docLine = new DocLine(docInvoice.DocumentType, docInvoice.Record_ID, "");
          docLine.m_C_Tax_ID = docInvoice.getM_taxes()[i].m_C_Tax_ID;
          // Timing of VAT
          fact.createLine(
              docLine,
              org.isObtvbIssettlementvat() ? OBTVB_Utility.getTransitoryTaxAccount(
                  docInvoice.getM_taxes()[i].m_C_Tax_ID, as, conn) : docInvoice.getM_taxes()[i]
                  .getAccount(DocTax.ACCTTYPE_TaxDue, as, conn), docInvoice.C_Currency_ID, "",
              new BigDecimal(docInvoice.getM_taxes()[i].m_amount).negate().toString(),
              Fact_Acct_Group_ID, docInvoice.nextSeqNo(SeqNo), docInvoice.DocumentType, conn);
        }
        // Revenue CR
        if (docInvoice.p_lines != null && docInvoice.p_lines.length > 0) {
          for (int i = 0; i < docInvoice.p_lines.length; i++)
            fact.createLine(docInvoice.p_lines[i], ((DocLine_Invoice) docInvoice.p_lines[i])
                .getAccount(ProductInfo.ACCTTYPE_P_Revenue, as, conn), docInvoice.C_Currency_ID,
                "", new BigDecimal(docInvoice.p_lines[i].getAmount()).negate().toString(),
                Fact_Acct_Group_ID, docInvoice.nextSeqNo(SeqNo), docInvoice.DocumentType, conn);
        }
        // Set Locations
        FactLine[] fLines = fact.getLines();
        for (int i = 0; i < fLines.length; i++) {
          if (fLines[i] != null) {
            fLines[i].setLocationFromOrg(fLines[i].getAD_Org_ID(conn), true, conn); // from Loc
            fLines[i].setLocationFromBPartner(docInvoice.C_BPartner_Location_ID, false, conn); // to
            // Loc
          }
        }
      } else {
        log4j.warn("Doc_Invoice - docInvoice.DocumentType unknown: " + docInvoice.DocumentType);
        fact = null;
      }
      SeqNo = "0";
      return fact;
    } finally {
      OBContext.restorePreviousMode();
    }
  }
}
